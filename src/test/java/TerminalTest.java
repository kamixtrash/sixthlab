import model.client.Client;
import model.client.Recipient;
import model.client.Sender;
import model.currency.Currency;
import model.terminal.FrodMonitor;
import model.terminal.PinValidator;
import model.terminal.TerminalImpl;
import model.terminal.TerminalServer;
import model.transaction.Transaction;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TerminalTest {

    static TerminalServer terminalServer;
    static PinValidator pinValidator;
    static FrodMonitor frodMonitor;
    static TerminalImpl terminal;
    static Client client1;
    static Client client2;
    static Client client3;

    @BeforeAll
    public static void prepareData() {
        terminalServer = new TerminalServer();
        pinValidator = new PinValidator();
        frodMonitor = new FrodMonitor(terminalServer);
        terminal = new TerminalImpl(terminalServer, pinValidator, frodMonitor);
        client1 = new Recipient("Ivanov", "Ivan", "Ivanovich", "RGNJ-1235-8452-RNJF", 345674, 1000L, "1233");
        client2 = new Sender("Petrov", "Petr", "Petrovich", "JTDS-2378-3412-OKAN", 124663, 2000L, "1234");
        client3 = new Sender("Scamer", "Scam", "Scam", "RYJN-3463-NJFF-4363", 436356, 10_000_000L, "5555");
    }

    @Test
    public void pinValidatorTest() {
        Transaction transaction = terminal.createTransaction(100L, Currency.USD, client1, client2, 1, "1234");
        assertTrue(terminal.checkTransaction(transaction, "1234", client2));
        assertFalse(terminal.checkTransaction(transaction, "1233", client2));
        assertFalse(terminal.checkTransaction(transaction, "1233", client2));
        assertFalse(terminal.checkTransaction(transaction, "1233", client2));
        LocalDateTime now = LocalDateTime.now();
        // wait 6 seconds.
        while (LocalDateTime.now().getSecond() - now.getSecond() < 6) {

        }
        // try with right pin.
        assertTrue(terminal.checkTransaction(transaction, "1234", client2));
    }

    @Test
    public void transactionInsFundsExceptionTest() {
        Transaction transaction1 = terminal.createTransaction(5000L, Currency.RUB, client1, client2, 2, "1234");
        assertFalse(terminal.commitTransaction(transaction1, "1234", client2));
    }

    @Test
    public void transactionWrongAmountTest() {
        Transaction transaction2 = terminal.createTransaction(125L, Currency.RUB, client1, client2, 2, "1234");
        assertFalse(terminal.checkTransaction(transaction2, "1234", client2));
    }

    @Test
    public void transactionFrodMonitorAmountTest() {
        Transaction transaction3 = terminal.createTransaction(10_000_000L, Currency.RUB, client2, client3, 1, "5555");
        assertFalse(terminal.checkTransaction(transaction3, "5555", client3));
    }

    @Test
    public void transactionFrodMonitorSenderAndRecipientTest() {
        ArrayList<Transaction> transactions = new ArrayList<>();
        for (int i = 0; i < 8; i++) {
            transactions.add(terminal.createTransaction(100_000L, Currency.RUB, client2, client3, 1, "5555"));
            terminal.checkTransaction(transactions.get(transactions.size() - 1), "5555", client3);
        }
        transactions.add(terminal.createTransaction(100_000L, Currency.RUB, client2, client3, 1, "5555"));
        assertFalse(terminal.checkTransaction(transactions.get(transactions.size()-1), "5555", client3));
        transactions.add(terminal.createTransaction(100_000L, Currency.RUB, client1, client3, 1, "5555"));
        assertFalse(terminal.checkTransaction(transactions.get(transactions.size() - 1), "5555", client3));
    }
}
