package model.transaction;

import com.google.gson.GsonBuilder;
import exceptions.InsufficientFundsException;
import exceptions.WrongAmountException;
import model.client.Client;
import model.currency.Currency;

import java.time.LocalDateTime;

/**
 * Абстрактный класс, представляющий собой перевод.
 */
public abstract class Transaction {
    private final Long id;
    private static Long transactionIdCounter = 0L;
    private final Long amount;
    private final Currency currency;
    private final Client recipient;
    private final Client sender;
    private final LocalDateTime transactionDate;
    private final boolean isWithdraw;

    public Transaction(Long amount, Currency currency, Client recipient, Client sender, boolean isWithdraw) {
        this.id = transactionIdCounter++;
        this.amount = amount;
        this.currency = currency;
        this.recipient = recipient;
        this.sender = sender;
        this.transactionDate = LocalDateTime.now();
        this.isWithdraw = isWithdraw;
    }

    public Client getRecipient() {
        return recipient;
    }

    public Client getSender() {
        return sender;
    }

    public Long getAmount() {
        return amount;
    }

    public LocalDateTime getTransactionDate() {
        return transactionDate;
    }

    @Override
    public String toString() {
        return new GsonBuilder().setPrettyPrinting().create().toJson(this);
    }

    /**
     * Валидация перевода.
     *
     * @return true в том случае, если сумма перевода строго больше нуля и на балансе достаточно средств для перевода.
     */
    public boolean check() {
        if (amount < 0 || amount % 100L != 0L) {
            throw new WrongAmountException();
        }
        if (sender.getBalance() < amount) {
            throw new InsufficientFundsException();
        }
        return true;
    }

    /**
     * Утверждение перевода после успешной валидации или же вывод информации о том, что на балансе недостаточно средств.
     */
    public boolean commit() throws InsufficientFundsException, WrongAmountException {
        if (check()) {
            sender.setBalance(sender.getBalance() - amount);
            recipient.setBalance(recipient.getBalance() + amount);
        } else if (isWithdraw) {
            sender.setBalance(sender.getBalance() - amount);
        } else {
            return false;
        }
        return true;
    }
}