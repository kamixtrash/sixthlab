package model.terminal;

import exceptions.AccountIsLockedException;
import exceptions.InvalidPinException;
import model.client.Client;

import java.time.LocalDateTime;

/**
 * Класс для валидации пин-кода.
 */
public class PinValidator {
    private boolean isBlocked;
    private Integer attempts;
    private LocalDateTime timeout;

    public PinValidator() {
        this.isBlocked = false;
        this.attempts = 0;
    }

    /**
     * Метод для валидации.
     * @param providedPin пин-код, введённый пользователем.
     * @param sender отправитель.
     */
    public void validate(String providedPin, Client sender) {
        if (!checkPin(providedPin, sender) && attempts < 3) {
            throw new InvalidPinException();
        }
        if (isTimedOut() && isBlocked) {
            throw new AccountIsLockedException();
        }
        if (!isTimedOut() && isBlocked) {
            unlockAccount();
            validate(providedPin, sender);
        }
    }

    /**
     * Метод для проверки введённого пин-кода.
     * @param providedPin пин-код, введённый пользователем.
     * @param sender пользователь-отправитель.
     * @return true, если пин-код совпадает. false, если нет.
     */
    private boolean checkPin(String providedPin, Client sender) {
        if (!sender.getPin().equals(providedPin)) {
            attempts++;
            blockAccount();
            return false;
        }
        return true;
    }

    /**
     * Метод для блокировки пользователя.
     */
    private void blockAccount() {
        if (attempts == 3) {
            timeout = LocalDateTime.now();
            isBlocked = true;
        }
    }

    /**
     * Метод для разблокировки пользователя.
     */
    private void unlockAccount() {
        isBlocked = false;
        attempts = 0;
    }

    /**
     * Метод для проверки блокировки по времени.
     * @return true, если с момента блокировки прошло меньше пяти секунд.
     */
    private boolean isTimedOut() {
        return getTimeoutLeft() < 5;
    }

    /**
     * Метод для получения разницы между текущим временем и временем,
     * когда была получена блокировка.
     * @return количество секунд.
     */
    public Integer getTimeoutLeft() {
        return timeout != null ? LocalDateTime.now().getSecond() - getTimeout().getSecond() : 100;
    }

    public LocalDateTime getTimeout() {
        return timeout;
    }

    public void setTimeout(LocalDateTime timeout) {
        this.timeout = timeout;
    }

    public Integer getAttempts() {
        return attempts;
    }

    public void setAttempts(Integer attempts) {
        this.attempts = attempts;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public void setBlocked(boolean blocked) {
        isBlocked = blocked;
    }
}
