package model.terminal;

import exceptions.*;
import model.client.Client;
import model.currency.Currency;
import model.transaction.Transaction;

/**
 * Класс имплементации терминала.
 */
public class TerminalImpl implements Terminal {
    private final TerminalServer terminalServer;
    private final PinValidator pinValidator;
    private final FrodMonitor frodMonitor;

    /**
     * @param terminalServer сервер, к которому подключается терминал.
     * @param pinValidator валидатор пин-кодов.
     * @param frodMonitor фрод-монитор.
     */
    public TerminalImpl(TerminalServer terminalServer, PinValidator pinValidator, FrodMonitor frodMonitor) {
        this.terminalServer = terminalServer;
        this.pinValidator = pinValidator;
        this.frodMonitor = frodMonitor;
    }

    @Override
    public Transaction createTransaction(Long amount, Currency currency, Client recipient, Client sender, Integer type, String providedPin) {
        try {
            validatePin(providedPin, sender);
            return terminalServer.createTransaction(amount, currency, recipient, sender, type);
        } catch (WrongAmountException e) {
            System.out.println("Operation canceled. Wrong Amount!");
            return null;
        } catch (AccountIsLockedException e) {
            System.out.println("Sorry, your account was blocked. Try again after " + pinValidator.getTimeoutLeft() + " second(s)");
            return null;
        } catch (InvalidPinException e) {
            System.out.println("Wrong pin! Try again. Attempts left: " + (3 - pinValidator.getAttempts()));
            return null;
        }
    }

    @Override
    public boolean checkTransaction(Transaction transaction, String providedPin, Client sender) {
        try {
            validatePin(providedPin, sender);
        } catch (AccountIsLockedException e) {
            System.out.println("Sorry, your account was blocked. Try again after " + pinValidator.getTimeout() + "second(s)");
            return false;
        } catch (InvalidPinException e) {
            System.out.println("Wrong pin! Try again. Attempts left: " + (3 - pinValidator.getAttempts()));
            return false;
        }

        try {
            return transaction.check() && frodMonitor.checkTransaction(transaction);
        } catch (InsufficientFundsException e) {
            System.out.println("Looks like you don't have enough money to perform this operation.");
            return false;
        } catch (WrongAmountException e) {
            System.out.println("Looks like you're trying to transfer wrong amount of money.");
            return false;
        } catch (SuspiciousSenderActivityException e) {
            System.out.println("Sorry, but we detected suspicious activity from you in this month. Contact support.");
            return false;
        } catch (SuspiciousRecipientActivityException e) {
            System.out.println("Sorry, but we detected suspicious activity from user in this month. Contact support.");
            return false;
        } catch (SuspiciousAmountException e) {
            System.out.println("Sorry, but we can't perform operations with this amount of money. Contact support.");
            return false;
        }
    }

    @Override
    public boolean commitTransaction(Transaction transaction, String providedPin, Client sender) {
        try {
            validatePin(providedPin, sender);
        } catch (AccountIsLockedException e) {
            System.out.println("Sorry, your account was blocked. Try again after " + pinValidator.getTimeout());
            return false;
        } catch (InvalidPinException e) {
            System.out.println("Wrong pin! Try again. Attempts left: " + (3 - pinValidator.getAttempts()));
            return false;
        }

        return checkTransaction(transaction, providedPin, sender) && terminalServer.commitTransaction(transaction);
    }

    @Override
    public void validatePin(String providedPin, Client sender) throws InvalidPinException, AccountIsLockedException {
        pinValidator.validate(providedPin, sender);
    }
}
