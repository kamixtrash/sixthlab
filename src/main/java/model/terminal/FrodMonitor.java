package model.terminal;

import exceptions.SuspiciousAmountException;
import exceptions.SuspiciousRecipientActivityException;
import exceptions.SuspiciousSenderActivityException;
import model.transaction.Transaction;

import java.time.LocalDateTime;

/**
 * Фрод-монитор предназначен для того,
 * чтобы отслеживать подозрительную активность от пользователей
 * и не давать им возможности проводить махинации.
 */
public class FrodMonitor {
    private final TerminalServer terminalServer;

    /**
     * @param terminalServer сервер терминала.
     */
    public FrodMonitor(TerminalServer terminalServer) {
        this.terminalServer = terminalServer;
    }

    /**
     * Метод для проверки транзакции.
     * @param transaction транзакция, которая должна быть проверена.
     * @return true, если транзакция прошла проверку. Иначе бросается соответствующее исключение.
     */
    public boolean checkTransaction(Transaction transaction) throws SuspiciousAmountException, SuspiciousRecipientActivityException, SuspiciousSenderActivityException {
        if (terminalServer.getTransactions() == null) {
            return checkAmount(transaction);
        }

        if (!checkRecipient(transaction)) {
            throw new SuspiciousRecipientActivityException();
        }
        if (!checkSender(transaction)) {
            throw new SuspiciousSenderActivityException();
        }
        if (!checkAmount(transaction)) {
            throw new SuspiciousAmountException();
        }

        return true;
    }

    /**
     * Метод для проверки отправителя.
     * @param transaction транзакция, в которой необходимо проверить отправителя.
     * @return true, если количество подозрительных транзакций меньше 10.
     */
    private boolean checkSender(Transaction transaction) {
        return terminalServer
                .getTransactions()
                .stream()
                .filter(transactionFromList -> transactionFromList.getSender().equals(transaction.getSender()))
                .filter(transactionFromList -> transactionFromList.getAmount() >= 100000L)
                .filter(transactionFromList -> transactionFromList.getTransactionDate().getMonth().equals(LocalDateTime.now().getMonth()))
                .count() < 10;
    }

    /**
     * Метод для проверки получателя.
     * @param transaction транзакция, в которой необходимо проверить получателя.
     * @return true, если количество подозрительных транзакций меньше 10.
     */
    private boolean checkRecipient(Transaction transaction) {
        return terminalServer
                .getTransactions()
                .stream()
                .filter(transactionFromList -> transactionFromList.getRecipient().equals(transaction.getRecipient()))
                .filter(transactionFromList -> transactionFromList.getAmount() >= 100000L)
                .filter(transactionFromList -> transactionFromList.getTransactionDate().getMonth().equals(LocalDateTime.now().getMonth()))
                .count() < 10;
    }

    /**
     * Метод для проверки суммы в транзакции.
     * @param transaction транзакция, в которой необходимо проверить сумму.
     * @return true, если сумма меньше 10 миллионов.
     */
    private boolean checkAmount(Transaction transaction) {
        return transaction.getAmount() < 10_000_000L;
    }
}
