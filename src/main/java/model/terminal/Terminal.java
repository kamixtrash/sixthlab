package model.terminal;

import model.client.Client;
import model.currency.Currency;
import model.transaction.Transaction;

public interface Terminal {
    /**
     * Метод для создания транзакции на основе входных параметров.
     * @param amount сумма перевода.
     * @param currency валюта, в которой будет выполнен перевод.
     * @param recipient получатель, который будет участвовать в переводе.
     * @param sender отправитель, который будет участвовать в переводе.
     * @param type тип перевода.
     * @param providedPin пин-код, введённый пользователем.
     * @return объект класса Transaction.
     */
    Transaction createTransaction(Long amount, Currency currency, Client recipient, Client sender, Integer type, String providedPin);

    /**
     * Метод для проверки транзакции.
     * @param transaction транзакция, которую нужно проверить.
     * @param providedPin предоставленный пин-код.
     * @param sender отправитель, участвующий в транзакции.
     * @return true, если транзакция успешно прошла проверку. В остальных случаях - false.
     */
    boolean checkTransaction(Transaction transaction, String providedPin, Client sender);

    /**
     * Метод для фиксации транзакции.
     * @param transaction транзакция, которая должна быть зафиксирована.
     * @param providedPin пин-код, введённый пользователем.
     * @param sender отправитель, участвующий в транзакции.
     * @return true, если транзакция прошла проверку и была успешно зафиксирована. Иначе - false.
     */
    boolean commitTransaction(Transaction transaction, String providedPin, Client sender);

    /**
     * Метод для валидации пин-кода.
     * @param providedPin пин-код, введённый пользователем.
     * @param sender отправитель, участвующий в переводе.
     */
    void validatePin(String providedPin, Client sender);
}
