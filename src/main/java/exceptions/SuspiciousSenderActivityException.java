package exceptions;

/**
 * Исключение, выбрасываемое в случае, когда получатель
 * замечен за подозрительной активностью.
 */
public class SuspiciousSenderActivityException extends RuntimeException {
    public SuspiciousSenderActivityException() {
    }

    public SuspiciousSenderActivityException(String message) {
        super(message);
    }

    public SuspiciousSenderActivityException(String message, Throwable cause) {
        super(message, cause);
    }

    public SuspiciousSenderActivityException(Throwable cause) {
        super(cause);
    }

    public SuspiciousSenderActivityException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
