package exceptions;

/**
 * Исключение, выбрасываемое в случае, когда сумма
 * перевода больше определённого числа.
 */
public class SuspiciousAmountException extends RuntimeException {
    public SuspiciousAmountException() {
    }

    public SuspiciousAmountException(String message) {
        super(message);
    }

    public SuspiciousAmountException(String message, Throwable cause) {
        super(message, cause);
    }

    public SuspiciousAmountException(Throwable cause) {
        super(cause);
    }

    public SuspiciousAmountException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
