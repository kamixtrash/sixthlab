package exceptions;

/**
 * Исключение, выбрасываемое в случае, когда получатель
 * замечен за подозрительной активностью.
 */
public class SuspiciousRecipientActivityException extends RuntimeException {
    public SuspiciousRecipientActivityException() {
    }

    public SuspiciousRecipientActivityException(String message) {
        super(message);
    }

    public SuspiciousRecipientActivityException(String message, Throwable cause) {
        super(message, cause);
    }

    public SuspiciousRecipientActivityException(Throwable cause) {
        super(cause);
    }

    public SuspiciousRecipientActivityException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
