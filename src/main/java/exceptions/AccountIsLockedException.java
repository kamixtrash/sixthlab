package exceptions;

/**
 * Исключение, выбрасываемое в случае, когда заблокированный
 * пользователь пытается выполнить какое-либо действие.
 */
public class AccountIsLockedException extends RuntimeException {
    public AccountIsLockedException() {
    }

    public AccountIsLockedException(String message) {
        super(message);
    }

    public AccountIsLockedException(String message, Throwable cause) {
        super(message, cause);
    }

    public AccountIsLockedException(Throwable cause) {
        super(cause);
    }

    public AccountIsLockedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
